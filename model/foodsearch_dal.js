var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.prepareSearch = function(callback) {
    var query = 'select food_class, count(*) as amount ' +
    'from menu_item ' +
    'group by food_class';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
}

exports.sendSearch = function (food_class, callback) {
    var query = 'Call serves_it(?)';
    var queryData = [food_class];

    connection.query(query, queryData, function (err, result) {
        callback(err, result);
    });
};