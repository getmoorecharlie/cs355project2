var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);


exports.getInfo = function (name, callback) {
    var query = 'SELECT r.name, l.* from restaurant r ' +
        'join location l on r.restaurant_id=l.restaurant_id ' +
        'where r.name=?';
    var queryData = [name]

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
}