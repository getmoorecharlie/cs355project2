var express = require('express');
var router = express.Router();
var foodsearch_dal = require('../model/foodsearch_dal');

router.get('/prepareSearch', function(req, res) {
    foodsearch_dal.prepareSearch(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('foodsearch/searchEntry', { 'menu_item':result });
        }
    });

});

router.get('/sendSearch', function (req, res) {
    foodsearch_dal.sendSearch(req.query.food_class, function (err, result) {
        res.render('foodsearch/searchResults', {result: result[0]});
    })
});

module.exports = router;
