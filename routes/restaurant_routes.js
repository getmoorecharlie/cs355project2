var express = require('express');
var router = express.Router();
var restaurant_dal = require('../model/restaurant_dal');

router.get('/locations', function(req, res) {
    if(req.query.name == null) {
        res.send('A restaurant name is required');
    }
    else {
        restaurant_dal.getInfo(req.query.name, function(err, result){
            res.render('restaurant/locationInfo', {result: result});
        });
    }
});

module.exports = router;